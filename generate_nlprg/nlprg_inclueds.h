# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include "./get_ancor_my.c"

#define COMPACT
//#define DIVERGENT
//#define STANDARD
//#define MINIMAL


#define ANCOR_CENTER
//#define ANCOR_END
//#define ANCOR_MID
//#define ANCOR_START

#ifdef ANCOR_START
  char * ancor = "ANCOR_START";
#endif
#ifdef ANCOR_END
  char * ancor = "ANCOR_END";
#endif
#ifdef ANCOR_MID
  char * ancor = "ANCOR_MID";
#endif
#ifdef ANCOR_CENTER
  char * ancor = "ANCOR_CENTER";
#endif

#ifdef COMPACT
  char * type = "COMPACT";
#endif

#ifdef DIVERGENT
  char * type = "DIVERGENT";
#endif

#ifdef STANDARD
  char * type = "STANDARD";
#endif

#ifdef MINIMAL
  char * type = "MINIMAL";
#endif