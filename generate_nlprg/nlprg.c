# include <stdint.h>
# include <stdio.h>
# include "./nlprg.h"

//# define SEED 0xA013 // any seed can work
# define SEED 0x0000 // any seed can work


// testbench for the non-linear pseudo-random generator
void simulate ( int * res )
{

    uint16_t reg        = SEED               ;
    int      period     = 0                  ;
    uint16_t verbose    = 1                  ;
    uint16_t diehard    = 0                  ;
    int      pass       = 0                  ;
    int      max_period = 2 << ( nbits - 1 ) ; 

    if ( diehard ) printf ("type: d\ncount: %d\nnumbit: %d\n", max_period , nbits);

    do {

        reg = nlprg_n ( reg ) ;

        if (( reg == SEED ) && 
               ( period == max_period - 1))  {
            pass  = 1 ;
        }

        period ++ ;


        if ( diehard ) printf ("%6d\n", reg );


    } while ( ( reg != SEED ) && ( period < max_period )) ;

  res[0] = pass;
  res[1] = period;
  return ; 

}

int main ( int argc , char * argv[] ) {

    int res [2] ;
    simulate(res) ;

    if ( res[0] ) { printf ("PASS %d\n",res[1]); } 
    else          { printf ("fail %d\n",res[1]); }

    return 0;
}
