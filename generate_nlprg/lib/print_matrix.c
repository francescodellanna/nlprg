#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_LENGTH 65536
#define mi  16
#define mj  16


int main (int argc , char * argv[] ){

  FILE * fp ;
  int i, j ;
  char m [mi][mj][5] ;
  char buffer [256] ;
  int num ;
  int pos ;

  char * file_name = "summary.tmp" ;

  sprintf(buffer,"/bin/sh ./summary.sh %s > summary.tmp", argv[1]);
  system(buffer);

  fp = fopen(file_name,"r") ;

  for ( i = 0; i < mi ; i ++ )
    for ( j = 0; j < mj ; j++ ){
      if ( i == 1 ){
         sprintf(buffer,"%3d",j);
         strcpy(m[i][j],buffer);
      } else {
         strcpy(m[i][j] , "   ") ;
      }
    } 

  while ( fscanf( fp , "%d %s", &num, buffer ) == 2) {

    if(!strcmp(buffer,"NBITS")){

      pos = num - 1;
      sprintf(buffer,"%3d",pos+1);
      strcpy(m[pos][0],buffer);

    } else if(!strcmp(buffer,"PASS")){

      strcpy(m[pos][num],"  x");

    }

  }

  for ( i = 1; i < mi ; i ++ ){
    for ( j = 0; j < mj ; j++ )
      printf("%s", m[i][j] ) ;
    printf("\n");
  } 

  system("rm summary.tmp");
  fclose(fp) ;
  return 0;
}