#include <string.h>

void get_ancor_my ( int * res , int nbits , char *ancor, char * type ){

    if ( ! strcmp( type , "COMPACT" ) )
       res [0] = ( nbits / 2 ) - 1;
    if ( ! strcmp( type , "MINIMAL" ) )
       res [0] =  2 ;
    if ( ! strcmp( type , "DIVERGENT" ) )
       res [0] =  nbits / 3 ;
    if ( ! strcmp( type , "STANDARD" ) )
       res [0] =  nbits / 3 ;


    if ( ! strcmp( ancor , "ANCOR_START" ) )
       res [1] = res[0] ;
    if ( ! strcmp( ancor , "ANCOR_MID" ) )
       res [1] = ((nbits - 1) - res[0] ) / 2 + res[0] ;
    if ( ! strcmp( ancor , "ANCOR_CENTER" ) )
       res [1] = nbits / 2 + ((nbits+1)%2);
    if ( ! strcmp( ancor , "ANCOR_END" ) )
       res [1] = nbits - 2 ;

  return ;

}

