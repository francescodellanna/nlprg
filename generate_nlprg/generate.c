#define x 8
#define nbits 10
#define COMPACT
//#define DIVERGENT
//#define STANDARD
//#define MINIMAL

//#define ANCOR_START
#define ANCOR_MID
//#define ANCOR_END
//#define ANCOR_CENTER

//# include "./type/nlprg_tmp.h"

# include <stdio.h>

// testbench for the non-linear pseudo-random generator

void generate_nlprg_p ( int (* m)[3] , int my )
{

  //preamble
  printf("# include <stdint.h>\n");
  printf("int nbits = %d ;\n", nbits );
  printf("uint16_t nlprg_p ( uint16_t num ) { return (\n" );

  int i;

  for ( i = 0; i < my ; i++ ){

    if (( i == 0 ) || ( i == my - 1 )){
       printf("(((~ ");
    } else {
       printf("(((  ");
    }

    if ( m[i][0] != -1 ){
      printf("( num >> %6d ) ^ ", m[i][0] );
    } else {
      printf("                    ", m[i][0] );
    }

    if ( m[i][1] != -1 ){
      printf("( num >> %6d ) ^ ", m[i][1] );
    } else {
      printf("( num >> %6d ) ^ ", i - 1 );
    }

    if (( m[i][2] != -1 ) || ( i == my - 1 )){

      if ( i == my - 1 ) {
        if ( x < 0 ) {  
          printf("( num >> %6d )   ", m[i][2] );
        } else {
                      printf("( num >> %6d )   ", x       );
        }
      } else {
                      printf("( num >> %6d )   ", m[i][2] );
      }
    } else {
      printf("( num >> %6d )   ", i - 1 );
    }

    printf(") & %#06x ) << %d ) ^ \n",1,i);
  }
  i = i - 2 ;

  if ( i != 0 ) {
    printf("(((                      ( num == %#06x ) | ( num == %#06x )   ) & %#06x ) << %d ) ^ \n", 
      ( 2 << i - 1 ) - 1 , 
      ( 2 << i     ) - 1 , 
      1, 
      i + 1);
  } else {
    printf("(((                      ( num == %#06x ) | ( num == %#06x )   ) & %#06x ) << %d ) ^ \n", 
       1 , 0 , 1 , i + 1);
  }

  printf("(( num & %#06x ) << %d )) & %#06x ; }\n",
     ( 2 << ( nbits - 1 ) ) - ( 2 << i ) ,
     1,
     ( 2 << ( nbits - 1 ) ) - 1); 

  return ; 

}

void generate_nlprg_n ( int (* m)[3] , int my )
{

  //preamble
  //printf("# include <stdint.h>\n");
  //printf("int nbits = %d ;\n", nbits );
  printf("\n\nuint16_t nlprg_n ( uint16_t num ) { return (\n" );

  int i;

  for ( i = 0; i < my ; i++ ){

    if (( i == 0 ) || ( i == my - 1 )){
       printf("(((  ");
    } else {
       printf("(((~ ");
    }

    if ( m[i][0] != -1 ){
      printf("( ~ (( num >> %6d ) ^ ", m[i][0] );
    } else {
      printf("                         ", m[i][0] );
    }

    if ( m[i][1] != -1 ){
      printf("( num >> %6d ))) ^ ", m[i][1] );
    } else {
      printf("( num >> %6d )   ^ ", i - 1 );
    }

    if (( m[i][2] != -1 ) || ( i == my - 1 )){

      if ( i == my - 1 ) {
        if ( x < 0 ) {  
          printf("( num >> %6d )   ", m[i][2] );
        } else {
                      printf("( num >> %6d )   ", x       );
        }
      } else {
                      printf("( num >> %6d )   ", m[i][2] );
      }
    } else {
      printf("( num >> %6d )   ", i - 1 );
    }

    printf(") & %#06x ) << %d ) ^ \n",1,i);
  }
  i = i - 2 ;

  if ( i != 0 ) {
    printf("(((                           ( num == %#06x )  |  ( num == %#06x )   ) & %#06x ) << %d ) ^ \n", 
     ( 2 << ( nbits - 1 ) ) - ( 2 << i ) ,
     ( 2 << ( nbits - 1 ) ) - ( 2 << ( i - 1) ) ,
     // ( 2 << i - 1 ) - 1 , 
     // ( 2 << i     ) - 1 , 
     1, 
     i + 1);
  } else {
    printf("(((                           ( num == %#06x )  |  ( num == %#06x )   ) & %#06x ) << %d ) ^ \n", 
       1 , 0 , 1 , i + 1);
  }

  printf("(( num & %#06x ) << %d )) & %#06x ; }\n",
     ( 2 << ( nbits - 1 ) ) - ( 2 << i ) ,
     1,
     ( 2 << ( nbits - 1 ) ) - 1); 

  return ; 

}

void generate_matrix ( int (*m)[3]  ){

    int i ;
    int my ;
    int j ;
    for ( i = 0 ; i < 512 ; i ++ ) {

       m[i][0] = -2 ;
       m[i][1] = -2 ;
       m[i][2] = -2 ;

    }


#ifdef COMPACT

       j = -1;
       my = ( nbits / 2 ) - 1;

       for ( i = 0 ; i < my + ( nbits % 2 ); i ++ ) {

          if ( i == 0 ) {

             m[i][0] = nbits + j ;
             j -- ;
             m[i][1] = nbits + j ;

#ifdef ANCOR_START
             m[i][2] = my ;
#endif
#ifdef ANCOR_MID
             m[i][2] = ((nbits - 1) - my ) / 2 + my ;
#endif
#ifdef ANCOR_CENTER
             m[i][2] = nbits / 2 + ((nbits+1)%2);
#endif
#ifdef ANCOR_END
             m[i][2] = nbits - 2 ;
#endif

          } else {


               m[i][0] = nbits + j ;
               j -- ;
               m[i][1] = nbits + j ;
               m[i][2] = - 1 ;


          }

          m[i+1][0] = - 1 ;
          m[i+1][1] = - 1 ;
          m[i+1][2] = - 1 ;

       }
#endif

#ifdef MINIMAL

       my =  2 ;
       j = 0;
       i = 0;


       m[i][0] = nbits - 1 ;
       m[i][1] = my ;

#ifdef ANCOR_START
       m[i][2] = my ;
#endif
#ifdef ANCOR_MID
       m[i][2] = ((nbits - 1) - my ) / 2 + my ;
#endif
#ifdef ANCOR_CENTER
       m[i][2] = nbits / 2 + ((nbits+1)%2);
#endif
#ifdef ANCOR_END
             m[i][2] = nbits - 2 ;
#endif

       m[i+1][0] = - 1 ;
       m[i+1][1] = - 1 ;
       m[i+1][2] = - 1 ;

#endif

#ifdef DIVERGENT

       my =  nbits / 3 ;
       j = 0;

       for ( i = 0 ; i < my ; i ++ ) {

          if ( i == 0 ) {

             m[i][0] = nbits - 1 ;
             m[i][1] = my ;

#ifdef ANCOR_START
             m[i][2] = my ;
#endif
#ifdef ANCOR_MID
             m[i][2] = ((nbits - 1) - my ) / 2 + my ;
#endif
#ifdef ANCOR_CENTER
       m[i][2] = nbits / 2 + ((nbits+1)%2);
#endif
#ifdef ANCOR_END
             m[i][2] = nbits - 2 ;
#endif
             j ++ ;

          } else {


               m[i][0] = nbits - 1 - j ;
               m[i][1] = my + j ;
               m[i][2] = - 1 ;
               j ++ ;


          }

          m[i+1][0] = - 1 ;
          m[i+1][1] = - 1 ;
          m[i+1][2] = - 1 ;

       }
#endif


#ifdef STANDARD

       my =  nbits / 3 ;
       j = 1;

       for ( i = 0 ; i < my ; i ++ ) {

          if ( i == 0 ) {

             m[i][0] = nbits - j ;
             j ++ ;
             m[i][1] = nbits - j ;
             j ++ ;

#ifdef ANCOR_START
             m[i][2] = my ;
#endif
#ifdef ANCOR_MID
             m[i][2] = ((nbits - 1) - my ) / 2 + my ;
#endif
#ifdef ANCOR_CENTER
             m[i][2] = nbits / 2 + ((nbits+1)%2);
#endif
#ifdef ANCOR_END
             m[i][2] = nbits - 2 ;
#endif

          } else {


             m[i][0] = nbits - j ;
             j ++ ;
             m[i][1] = nbits - j ;
             j ++ ;
             m[i][2] = - 1 ;


          }

          m[i+1][0] = - 1 ;
          m[i+1][1] = - 1 ;
          m[i+1][2] = - 1 ;

       }
#endif

  return ;

}


void print_m( int (* m)[3] , int my ){

  int i ;
  printf("\n//my = %d",my);
  for ( i = 0 ;  i < my  ; i++){
     printf("\n//%d %d %d",m[i][0],m[i][1],m[i][2]);
  }
  printf("\n");

}


int main ( ) {

    int m [512][3] ;
    int my = -1;
    int i ;

    generate_matrix ( m );

    for ( i = 0; i < 512 ; i++)
       if (( my == -1 ) && ( m [i][0] == -2 )) my = i ; 

    print_m( m , my);


    generate_nlprg_p ( m , my ) ;
    generate_nlprg_n ( m , my ) ;

    return 0;
}