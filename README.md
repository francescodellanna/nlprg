# NLPRG

This project is a collection of circuits that can produce a pseudo-random sequence with a period of 2^n numbers, where n is the numbers of registers. 
For more information visit my blog at https://fdblog.xyz/circuits/non-linear-pseudo-random-generator-introduction/

You can find all the drawn circuit in the doc folder.

The 8 bit testbench can be modified to test all the other 8 bit rtl modules. The instantiation of the module itself needs to be modified and the rest should work.

After running the simulation ( for example by running : iverilog nlprng8_tb.v ; ./a.out ), you should find the resulting sequence in the tb/log and all the waves in the tb/wave.

In the c folder there is a c code implementation of the nlprg and the C implemenation of the lfsr.
The proposed topology ( 16 bits with that fixed xor parallela plane) allows more closure connections, which are left as comments in the code. 
The equivalent circuit diagrams can be found in c/nlprg16/doc. The dieharder test suite has been run on the produced pseudo-random sequencies.
The results are summarized in c/nlprg16/res.txt.

In the example folder you can find a library of diverse nlprg ranging from 3 bits up to 16 bits.