# include <stdint.h>
# include <stdio.h>

# define SEED 0xA013 // any seed can work

// bijective pseudo-random funcion
uint16_t nlprg16 ( uint16_t num )
{
    return
        (
        (((~ ( num >> 15 ) ^ ( num >> 14 ) ^ ( num >> 5 ) ) & 0x0001 ) << 0 ) ^ // xnor stopper
        (((  ( num >> 13 ) ^ ( num >> 12 ) ^ ( num >> 0 ) ) & 0x0001 ) << 1 ) ^
        (((  ( num >> 11 ) ^ ( num >> 10 ) ^ ( num >> 1 ) ) & 0x0001 ) << 2 ) ^
        (((  ( num >> 9  ) ^ ( num >> 8  ) ^ ( num >> 2 ) ) & 0x0001 ) << 3 ) ^
        (((  ( num >> 7  ) ^ ( num >> 6  ) ^ ( num >> 3 ) ) & 0x0001 ) << 4 ) ^
        (((~ ( num >> 5  ) ^ ( num >> 4  )                ) & 0x0001 ) << 5 ) ^ // closure connection
        //(((~ ( num >> 7  ) ^ ( num >> 4  )                ) & 0x0001 ) << 5 ) ^ // alternative closure connection #1
        //(((~ ( num >> 13 ) ^ ( num >> 4  )                ) & 0x0001 ) << 5 ) ^ // alternative closure connection #2
        (((  ( num == 0x001F ) | ( num == 0x000F )        ) & 0x0001 ) << 5 ) ^ // insert missing code 0x0031
        (( num & 0xFFE0) << 1 ) // shift for all the other bits 
        ) ;

}

// used to hide the 0x000F -> 0x001F -> 0x003F transition
uint16_t shuffle ( uint16_t num ) 
{
    return
        (
        ((( num >> 15 ) & 0x0001 ) << 5  ) ^
        ((( num >> 14 ) & 0x0001 ) << 8  ) ^
        ((( num >> 13 ) & 0x0001 ) << 15 ) ^
        ((( num >> 12 ) & 0x0001 ) << 10 ) ^
        ((( num >> 11 ) & 0x0001 ) << 9  ) ^
        ((( num >> 10 ) & 0x0001 ) << 12 ) ^
        ((( num >> 9  ) & 0x0001 ) << 13 ) ^
        ((( num >> 8  ) & 0x0001 ) << 6  ) ^
        ((( num >> 7  ) & 0x0001 ) << 11 ) ^
        ((( num >> 6  ) & 0x0001 ) << 0  ) ^
        ((( num >> 5  ) & 0x0001 ) << 2  ) ^
        ((( num >> 4  ) & 0x0001 ) << 14 ) ^
        ((( num >> 3  ) & 0x0001 ) << 1  ) ^
        ((( num >> 2  ) & 0x0001 ) << 4  ) ^
        ((( num >> 1  ) & 0x0001 ) << 3  ) ^
        ((( num >> 0  ) & 0x0001 ) << 7  ) 
        );

}

// testbench for the non-linear pseudo-random generator
int main ()
{

    uint16_t nlprg   = SEED ;
    int      period  = 0    ;
    int      verbose = 1    ;
    

    if ( verbose ) printf ("type: d\ncount: 65536\nnumbit: 16\n");

    do {

        nlprg = nlprg16 ( nlprg ) ;
        period ++ ;

        //if ( verbose ) printf ("period %6d   nlprg %6d\n", period, shuffle ( nlprg ) );
        if ( verbose ) printf ("%6d\n", shuffle(nlprg)  );

    } while ( nlprg != SEED ) ;

  return 0 ; 

}