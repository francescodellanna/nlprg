# include <stdint.h>
# include <stdio.h>

uint16_t shuffle ( uint16_t num ) 
{
    return
        (
        ((( num >> 15 ) & 0x0001 ) << 5  ) ^
        ((( num >> 14 ) & 0x0001 ) << 8  ) ^
        ((( num >> 13 ) & 0x0001 ) << 15 ) ^
        ((( num >> 12 ) & 0x0001 ) << 10 ) ^
        ((( num >> 11 ) & 0x0001 ) << 9  ) ^
        ((( num >> 10 ) & 0x0001 ) << 12 ) ^
        ((( num >> 9  ) & 0x0001 ) << 13 ) ^
        ((( num >> 8  ) & 0x0001 ) << 6  ) ^
        ((( num >> 7  ) & 0x0001 ) << 11 ) ^
        ((( num >> 6  ) & 0x0001 ) << 0  ) ^
        ((( num >> 5  ) & 0x0001 ) << 2  ) ^
        ((( num >> 4  ) & 0x0001 ) << 14 ) ^
        ((( num >> 3  ) & 0x0001 ) << 1  ) ^
        ((( num >> 2  ) & 0x0001 ) << 4  ) ^
        ((( num >> 1  ) & 0x0001 ) << 3  ) ^
        ((( num >> 0  ) & 0x0001 ) << 7  ) 
        );

}


int main( )
{
    uint16_t start_state = 0xACE1u;  /* Any nonzero start state will work. */
    uint16_t lfsr = start_state;
    uint16_t bit;                    /* Must be 16-bit to allow bit<<15 later in the code */
    unsigned period = 0;

    printf("type: d\ncount: 65535\nnumbit: 16\n");

    do
    {   /* taps: 16 14 13 11; feedback polynomial: x^16 + x^14 + x^13 + x^11 + 1 */
        bit = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5)) /* & 1u */;
        lfsr = (lfsr >> 1) | (bit << 15);
        ++period;

        printf("%d\n",shuffle(lfsr));

    }
    while (lfsr != start_state);

    return period;
}
